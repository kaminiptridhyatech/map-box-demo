# mapbox_demo

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Authentication:
- [Create MapBox Account](https://account.mapbox.com/)
- [Create an access token](https://account.mapbox.com/access-tokens/create) and style ID
- Select [MapBox Studio](https://studio.mapbox.com/)
- Choose your style and click share button click on the Third-Party tab
- Here, select the CARTO option, and copy the Integration URL


## Integration:
- Add the latest version(flutter_map:,flutter_mapbox_navigation:,mapbox_gl:)
- import file
- Customize as per your requirements