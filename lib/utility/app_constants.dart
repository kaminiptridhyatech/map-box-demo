import 'package:latlong2/latlong.dart';

class AppConstants {
  static const String mapBoxAccessToken =
      'sk.eyJ1IjoidHJpZGh5YS1tYXBib3gtdGVzdCIsImEiOiJjbDZuOHl3bGgwYjJmM2NvZjdlbTVyd3V1In0.gWqVHyj2-O4ES6ppVUrXog';

  static const String mapBoxStyleId = 'mapbox.mapbox-streets-v8';

  static final myLocation = LatLng(23.022505, 72.571362);
 static var points = <LatLng> [
    LatLng(23.012441, 72.511223),
    LatLng(23.033962, 72.463792),
    // LatLng(23.069339, 72.673573),
    // LatLng(23.129891, 72.584963),
    // LatLng(23.040508, 72.504783),
  ];
}
