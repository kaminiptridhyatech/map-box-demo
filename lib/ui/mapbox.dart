import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_mapbox_navigation/library.dart';
import 'package:latlong2/latlong.dart';
import 'package:permission_handler/permission_handler.dart';
import '../model/map_marker_model.dart';
import '../utility/app_constants.dart';

class MapBoxDemo extends StatefulWidget {
  const MapBoxDemo({Key? key}) : super(key: key);

  @override
  State<MapBoxDemo> createState() => _MapBoxDemoState();
}

class _MapBoxDemoState extends State<MapBoxDemo> with TickerProviderStateMixin {
  final pageController = PageController();
  int selectedIndex = 0;
  var currentLocation = AppConstants.myLocation;
  var polylines = AppConstants.points;
  late final MapController mapController;
  String _platformVersion = 'Unknown';
  String _instruction = "";
  final _origin =
      WayPoint(name: "Way Point 1", latitude: 23.012441, longitude: 72.511223);
  final _stop1 =
      WayPoint(name: "Way Point 2", latitude: 23.033962, longitude: 72.463792);
  final _stop2 =
      WayPoint(name: "Way Point 3", latitude: 23.069339, longitude: 72.673573);
  final _stop3 =
      WayPoint(name: "Way Point 4", latitude: 23.129891, longitude: 72.584963);
  final _stop4 =
      WayPoint(name: "Way Point 5", latitude: 23.040508, longitude: 72.504783);

  MapBoxNavigation? _directions;
  MapBoxOptions? _options;

  bool _isMultipleStop = false;
  double _distanceRemaining = 0.0, _durationRemaining = 0.0;
  MapBoxNavigationViewController? _controller;
  bool _routeBuilt = false;
  bool _isNavigating = false;
  void wayPoints() async {
    await Permission.locationAlways.request();
    print(Permission.locationAlways.status.then((value) => print(value)));
  }

  @override
  void initState() {
    super.initState();
    mapController = MapController();
    initialize();
  }

  Future<void> initialize() async {
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    _directions = MapBoxNavigation(onRouteEvent: _onEmbeddedRouteEvent);
    _options = MapBoxOptions(
        //initialLatitude: 36.1175275,
        //initialLongitude: -115.1839524,
        zoom: 15.0,
        tilt: 0.0,
        bearing: 0.0,
        enableRefresh: false,
        alternatives: true,
        voiceInstructionsEnabled: true,
        bannerInstructionsEnabled: true,
        allowsUTurnAtWayPoints: true,
        mode: MapBoxNavigationMode.drivingWithTraffic,
        units: VoiceUnits.imperial,
        simulateRoute: false,
        animateBuildRoute: true,
        longPressDestinationEnabled: true,
        language: "en");

    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await _directions!.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child:
          // Scaffold(
          //   body: Stack(
          //     children: [
          //       FlutterMap(
          //         mapController: mapController,
          //         options: MapOptions(
          //           minZoom: 5,
          //           maxZoom: 18,
          //           zoom: 11,
          //           center: currentLocation,
          //         ),
          //         layers: [
          //           TileLayerOptions(
          //             urlTemplate:
          //             "https://api.mapbox.com/styles/v1/tridhya-mapbox-test/cl6g4c5s8000z14rj1digei6p/tiles/256/{z}/{x}/{y}@2x?access_token=sk.eyJ1IjoidHJpZGh5YS1tYXBib3gtdGVzdCIsImEiOiJjbDZuOHl3bGgwYjJmM2NvZjdlbTVyd3V1In0.gWqVHyj2-O4ES6ppVUrXog",
          //             additionalOptions: {
          //               'mapStyleId': AppConstants.mapBoxStyleId,
          //               'accessToken': AppConstants.mapBoxAccessToken,
          //             },
          //           ),
          //           MarkerLayerOptions(
          //             markers: [
          //               for (int i = 0; i < mapMarkers.length; i++)
          //                 Marker(
          //                   height: 40,
          //                   width: 40,
          //                   point: mapMarkers[i].location ?? AppConstants.myLocation,
          //                   builder: (_) {
          //                     return GestureDetector(
          //                       onTap: () {
          //                         pageController.animateToPage(
          //                           i,
          //                           duration: const Duration(milliseconds: 500),
          //                           curve: Curves.easeInOut,
          //                         );
          //                         selectedIndex = i;
          //                         currentLocation = mapMarkers[i].location ??
          //                             AppConstants.myLocation;
          //                         _animatedMapMove(currentLocation, 11.5);
          //                         setState(() {});
          //                       },
          //                       child: AnimatedScale(
          //                         duration: const Duration(milliseconds: 500),
          //                         scale: selectedIndex == i ? 1 : 0.7,
          //                         child: AnimatedOpacity(
          //                           duration: const Duration(milliseconds: 500),
          //                           opacity: selectedIndex == i ? 1 : 0.5,
          //                           child: Image.asset(
          //                             'assets/icons/location.png',
          //                           ),
          //                         ),
          //                       ),
          //                     );
          //                   },
          //                 ),
          //             ],
          //           ),
          //           PolylineLayerOptions(
          //               polylines: [
          //                 Polyline(points: polylines,
          //                     strokeWidth: 5,
          //                     color: Colors.red)
          //               ]
          //           )
          //         ],
          //       ),
          //       Positioned(
          //         left: 0,
          //         right: 0,
          //         bottom: 2,
          //         height: MediaQuery.of(context).size.height / 3.2,
          //         child: PageView.builder(
          //           controller: pageController,
          //           onPageChanged: (value) {
          //             selectedIndex = value;
          //             currentLocation =
          //                 mapMarkers[value].location ?? AppConstants.myLocation;
          //             _animatedMapMove(currentLocation, 11.5);
          //             setState(() {});
          //           },
          //           itemCount: mapMarkers.length,
          //           itemBuilder: (_, index) {
          //             final item = mapMarkers[index];
          //             return Padding(
          //               padding: const EdgeInsets.all(15.0),
          //               child: Card(
          //                 elevation: 5,
          //                 shape: RoundedRectangleBorder(
          //                   borderRadius: BorderRadius.circular(10),
          //                 ),
          //                 color: Colors.black,
          //                 child: Row(
          //                   children: [
          //                     const SizedBox(width: 10),
          //                     Expanded(
          //                       child: Column(
          //                         mainAxisAlignment: MainAxisAlignment.center,
          //                         children: [
          //                           Expanded(
          //                             child: ListView.builder(
          //                               padding: EdgeInsets.zero,
          //                               scrollDirection: Axis.horizontal,
          //                               itemCount: item.rating,
          //                               itemBuilder:
          //                                   (BuildContext context, int index) {
          //                                 return const Icon(
          //                                   Icons.star,
          //                                   color: Colors.orange,
          //                                 );
          //                               },
          //                             ),
          //                           ),
          //                           Expanded(
          //                             flex: 2,
          //                             child: Column(
          //                               crossAxisAlignment: CrossAxisAlignment.start,
          //                               children: [
          //                                 Text(
          //                                   item.title ?? '',
          //                                   style: const TextStyle(
          //                                     fontSize: 20,
          //                                     color: Colors.white,
          //                                     fontWeight: FontWeight.bold,
          //                                   ),
          //                                 ),
          //                                 const SizedBox(height: 10),
          //                                 Text(
          //                                   item.address ?? '',
          //                                   style: const TextStyle(
          //                                     fontSize: 14,
          //                                     color: Colors.grey,
          //                                   ),
          //                                 ),
          //                               ],
          //                             ),
          //                           ),
          //                         ],
          //                       ),
          //                     ),
          //                     const SizedBox(width: 10),
          //                     Expanded(
          //                       child: Padding(
          //                         padding: const EdgeInsets.all(4.0),
          //                         child: ClipRRect(
          //                           borderRadius: BorderRadius.circular(10),
          //                           child: Image.asset(
          //                             item.image ?? '',
          //                             fit: BoxFit.cover,
          //                           ),
          //                         ),
          //                       ),
          //                     ),
          //                     const SizedBox(width: 10),
          //                   ],
          //                 ),
          //               ),
          //             );
          //           },
          //         ),
          //       )
          //     ],
          //   ),
          // )
          Scaffold(
        appBar: AppBar(
          // title: const Text('Plugin example app'),
        ),
        body: Column(children: <Widget>[
          SingleChildScrollView(
            child: Column(
              children: [
                ElevatedButton(
                  child: Text("Start"),
                  onPressed: () async {
                    var wayPoints = <WayPoint>[];
                    wayPoints.add(_origin);
                    wayPoints.add(_stop1);

                    await _directions!.startNavigation(
                        wayPoints: wayPoints,
                        options: MapBoxOptions(
                            mode:
                                MapBoxNavigationMode.drivingWithTraffic,
                            simulateRoute: false,
                            language: "en",
                            units: VoiceUnits.metric));
                  },
                ),
                Padding(
                  padding: EdgeInsets.only(
                      left: 20.0, right: 20, top: 20, bottom: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Text("Duration Remaining: "),
                          Text(_durationRemaining != null
                              ? "${(_durationRemaining / 60).toStringAsFixed(0)} minutes"
                              : "---")
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Text("Distance Remaining: "),
                          Text(_distanceRemaining != null
                              ? "${(_distanceRemaining * 0.000621371).toStringAsFixed(1)} miles"
                              : "---")
                        ],
                      ),
                    ],
                  ),
                ),
                Divider()
              ],
            ),
          ),
          Expanded(flex: 2,
            child: MapBoxNavigationView(
                options: _options,
                onRouteEvent: _onEmbeddedRouteEvent,
                onCreated:
                    (MapBoxNavigationViewController controller) async {
                  _controller = controller;
                  controller.initialize();
                }),
          )
        ]),
      ),
    );
  }

  Future<void> _onEmbeddedRouteEvent(e) async {
    _distanceRemaining = await _directions!.distanceRemaining;
    _durationRemaining = await _directions!.durationRemaining;

    switch (e.eventType) {
      case MapBoxEvent.progress_change:
        var progressEvent = e.data as RouteProgressEvent;
        if (progressEvent.currentStepInstruction != null)
          _instruction = progressEvent.currentStepInstruction!;
        break;
      case MapBoxEvent.route_building:
      case MapBoxEvent.route_built:
        setState(() {
          _routeBuilt = true;
        });
        break;
      case MapBoxEvent.route_build_failed:
        setState(() {
          _routeBuilt = false;
        });
        break;
      case MapBoxEvent.navigation_running:
        setState(() {
          _isNavigating = true;
        });
        break;
      case MapBoxEvent.on_arrival:
        if (!_isMultipleStop) {
          await Future.delayed(Duration(seconds: 3));
          await _controller!.finishNavigation();
        } else {}
        break;
      case MapBoxEvent.navigation_finished:
      case MapBoxEvent.navigation_cancelled:
        setState(() {
          _routeBuilt = false;
          _isNavigating = false;
        });
        break;
      default:
        break;
    }
    setState(() {});
  }
  void _animatedMapMove(LatLng destLocation, double destZoom) {
    final latTween = Tween<double>(
        begin: mapController.center.latitude, end: destLocation.latitude);
    final lngTween = Tween<double>(
        begin: mapController.center.longitude, end: destLocation.longitude);
    final zoomTween = Tween<double>(begin: mapController.zoom, end: destZoom);

    var controller = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);

    Animation<double> animation =
    CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn);

    controller.addListener(() {
      mapController.move(
        LatLng(latTween.evaluate(animation), lngTween.evaluate(animation)),
        zoomTween.evaluate(animation),
      );
    });

    animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controller.dispose();
      } else if (status == AnimationStatus.dismissed) {
        controller.dispose();
      }
    });

    controller.forward();
  }
}
