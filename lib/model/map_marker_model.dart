import 'package:latlong2/latlong.dart';

class MapMarker {
  final String? image;
  final String? title;
  final String? address;
  final LatLng? location;
  final int? rating;

  MapMarker({
    required this.image,
    required this.title,
    required this.address,
    required this.location,
    required this.rating,
  });
}

final mapMarkers = [
  MapMarker(
      image: 'assets/images/restaurant_1.jpg',
      title: 'Jassi De Parathe',
      address:
          "107, Safal Pegasus, 100 Feet Anand Nagar Rd, above McDonald's, Prahlad Nagar, Ahmedabad",
      location: LatLng(23.012441, 72.511223),
      rating: 4),
  MapMarker(
      image: 'assets/images/restaurant_2.jpg',
      title: 'Octant Pizza',
      address:
          '3rd Floor, One World West, Ambli T-Junction, 200, Sardar Patel Ring Rd, Bopal, Ahmedabad',
      location: LatLng(23.033962, 72.463792),
      rating: 3),
  MapMarker(
      image: 'assets/images/restaurant_3.jpg',
      title: 'Mayur Restaurant & Banquet',
      address:
          'Mayur restaurant nr. Man petrol Suthar karkhana naroda galaxy, Ahmedabad',
      location: LatLng(23.069339, 72.673573),
      rating: 4),
  MapMarker(
      image: 'assets/images/restaurant_4.jpg',
      title: "Nini's Kitchen (Chandkheda)",
      address:
          '3, Amrakunj Avis Nr. Tapovan Circle Visat - Gandhinagar Highway, Chandkheda, Ahmedabad',
      location: LatLng(23.129891, 72.584963),
      rating: 5),
  MapMarker(
    image: 'assets/images/restaurant_5.jpg',
    title: 'Taj Skyline',
    address:
        'The Taj Skyline, Sindhu Bhavan Marg, PRL Colony, Thaltej, Ahmedabad',
    location: LatLng(23.040508, 72.504783),
    rating: 4,
  ),
];
